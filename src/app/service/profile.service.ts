import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Profile } from '../model/profile';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  formData: Profile ;
  constructor(private firestore: AngularFirestore) { }
  getProfiles(){
    return this.firestore.collection('profiles').snapshotChanges();
  }
}
