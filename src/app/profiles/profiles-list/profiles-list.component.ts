import { Component, OnInit } from '@angular/core';
import{Profile} from 'src/app/model/profile'
import { ProfileService } from 'src/app/service/profile.service';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-profiles-list',
  templateUrl: './profiles-list.component.html',
  styleUrls: ['./profiles-list.component.css']
})
export class ProfilesListComponent implements OnInit {
  list : Profile[] ;
  constructor(private service :ProfileService,
    private firestore : AngularFirestore) { }

  ngOnInit() {
    this.service.getProfiles().subscribe(actionArray => {
      this.list = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data
        } as Profile;
         
      })
      
    });
    
    
  }
  onEdit(pro: Profile) {
    this.service.formData = Object.assign({},pro);
  }

  onDelete(id: string) {
    if (confirm("Are you sure to delete this record?")) {
      this.firestore.doc('Prifiles/' + id).delete();
     
    }
  }

}
