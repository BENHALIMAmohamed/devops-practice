// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false , 
  firebaseConfig :{
    apiKey: "AIzaSyDdNWCseJ0TgWNBq2e7rZyZggiHpYhBkNs",
    authDomain: "firestrorecloud.firebaseapp.com",
    databaseURL: "https://firestrorecloud.firebaseio.com",
    projectId: "firestrorecloud",
    storageBucket: "firestrorecloud.appspot.com",
    messagingSenderId: "895225320472",
    appId: "1:895225320472:web:0847db4566a79b0b79df6d",
    measurementId: "G-F1DVHQC9T9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
